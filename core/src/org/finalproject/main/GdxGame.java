package org.finalproject.main;

import org.finalproject.screens.ScreenList;
import org.finalproject.screens.ScreenManager;

import com.badlogic.gdx.Game;


public class GdxGame extends Game {

	
	@Override
	public void create () {
        ScreenManager.getInstance().initialize(this);
        ScreenManager.getInstance().show(ScreenList.MAIN_MENU);
		
	}

    @Override
    public void dispose() {
        super.dispose();
        ScreenManager.getInstance().dispose();
    }
	
	
	
}
